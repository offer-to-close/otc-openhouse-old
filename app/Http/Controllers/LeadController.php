<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadSignIn;
use App\Http\Requests\LeadSignOut;
use App\Models\Lead;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class LeadController extends Controller_Parent
{
    function __construct()
    {
        $this->model = new Lead();
        parent::__construct();
    }

    /**
     * Deprecated. Use UserController@getLeads()
     * @return mixed
     */
    public function deprecated_byUser()
    {
        $user = User::find(auth()->id());
        $leads = $user->leads()->get();
        foreach($leads as $lead)
        {
            $lead = Lead::find($lead->id);
            $lead->property = $lead->property()->get();
        }
        return $leads;
    }

    public function signIn(LeadSignIn $request)
    {
        $validated = $request->validated();
        $validated['status'] = 1;
        $this->updateOrInsert($validated);
        return redirect()->back();
    }

    public function signOut(LeadSignOut $request)
    {
        $validated = $request->validated();
        $validated['status'] = 2;
        $this->updateOrInsert($validated);
        return redirect()->back();
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $data['phone'] = preg_replace('/[^0-9]/i', '', $data['phone']);
        $this->updateOrInsert($data);
        return redirect()->back();
    }
}
