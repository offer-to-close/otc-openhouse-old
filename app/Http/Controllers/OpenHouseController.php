<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/15/2019
 * Time: 5:02 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\AddOpenHouse;
use App\Library\Utilities\_LaravelTools;
use App\Models\OpenHouse;
use App\Models\Property;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class OpenHouseController extends Controller_Parent
{
    protected $comments;
    protected $checkIns;
    protected $checkOuts;

    function __construct()
    {
        $this->model = new OpenHouse();
        parent::__construct();
    }

    public function add(AddOpenHouse $request)
    {
        $validated = $request->validated();
        $this->model->upsert($validated);
        return redirect()->route('get.page', ['name' => 'openHouses']);
    }

    /**
     * Deprecated, use UserController@getOpenHouses() instead.
     * @return array
     */
    public function deprecated_byUser()
    {
        $user = User::find(\auth()->id());
        $openHouses = [];
        if ($user)
        {
            $openHouses = $user->openHouses()
                ->select(
                    'open_house.*',
                    'property_details.address'
                )
                ->orderBy('open_house.date', 'asc')
                ->get();
        }
        return self::openHousesByTimeInterval($openHouses);
    }

    /**
     * This function requires $this->record to exist.
     * To be used with method chaining.
     * @return $this
     */
    public function withProperty()
    {
        $property = Property::find($this->record->property_id);
        $propertyFields = [
            'address',
            'agent_name',
            'price',
            'type',
            'square_footage',
            'bedrooms',
            'bathrooms'
        ];
        if($property) foreach ($propertyFields as $field) $this->record->{$field} = $property->{$field};
        else foreach ($propertyFields as $field) $this->record->{$field} = NULL;

        return $this;
    }

    /**
     * This function requires $this->record to exist.
     * To be used with method chaining.
     * @return $this
     */
    public function withSignedInLeads()
    {
        $this->record->leads = $this->record->leads()->where('status', 1)->get();

        return $this;
    }

    /**
     * Controller_Parent Constructor Explanation.
     * ________________________________________________
     * Inside of classes that extend 'Controller_Parent' class, some child class methods may require the $id parameter
     * but appear to not use the $id parameter inside of the method. The way that those classes work is as follows:
     * Laravel automatically instantiates an object of the class when a method is used within that class, inside of
     * Controller_Parent child class constructors, the constructor assigns $this->model to be the model relevant to that
     * class. It also calls Controller_Parent constructor. Since it is not possible to inject the $id parameter directly
     * into Controller_Parent constructor, we look at the route and see if there is an 'id' parameter, if there is we
     * assign $this->id to it and fetch the relevant record so that Controller_Parent child classes can use the data.
     * This is why it is still necessary for methods in child classes that will be working with specific records to
     * have an $id parameter.
     *
     * Children classes of Controller_Parent that do not have any sort of id parameter are either to be used for method
     * chaining or are being used for post requests (adding new data, etc.)
     */

    /**
     * @param $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signIn($id)
    {
        $view = 'prelogin.openHouseSignIn';
        return view(_LaravelTools::addVersionToViewName($view), [
            'data' => $this->withProperty()->record(),
        ]);
    }

    /**
     * @param $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function signOut($id)
    {
        $view = 'prelogin.openHouseSignOut';
        return view(_LaravelTools::addVersionToViewName($view), [
            'data' => $this->withProperty()->withSignedInLeads()->record(),
        ]);
    }

    /**
     * @param int $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return $this
     */
    public function getComments($id = 0)
    {
        $this->comments = collect();
        if (!$id) $id = $this->id;
        else $this->id = $id;
        if($id)
        {
            $openHouse = OpenHouse::find($id);
            if($openHouse)
            {
                $this->comments = $openHouse->leads()->select('feedback')->where('feedback','!=',NULL)->get();
            }
        }

        return $this;
    }

    /**
     * @param int $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return $this
     */
    public function getCheckIns($id = 0)
    {
        $this->checkIns = collect();
        if (!$id) $id = $this->id;
        else $this->id = $id;
        if($id)
        {
            $openHouse = OpenHouse::find($id);
            if($openHouse) $this->checkIns = $openHouse->leads()->get();
        }

        return $this;
    }

    /**
     * @param int $id (please read Controller_Parent Constructor Explanation to understand purpose of this param)
     * @return $this
     */
    public function getCheckOuts($id = 0)
    {
        $this->checkIns = collect();
        if (!$id) $id = $this->id;
        else $this->id = $id;
        if($id)
        {
            $openHouse = OpenHouse::find($id);
            if($openHouse) $this->checkOuts = $openHouse->leads()->where('status', '2')->get();
        }

        return $this;
    }

    /**
     * openHouses is a collection
     * @param $openHouses
     * @return array
     */
    public static function openHousesByTimeInterval($openHouses)
    {
        $oh = [
            'upcoming'  => [],
            'pastDue'   => [],
            'all'       => [],
        ];
        $today = strtotime(date('Y-m-d'));
        foreach ($openHouses as $openHouse)
        {
            $date = strtotime(date($openHouse->date));
            if($date < $today)
            {
                $openHouseRecord = new OpenHouseController();
                $openHouse->comments = $openHouseRecord->getComments($openHouse->id)->comments;
                $openHouse->checkIns = $openHouseRecord->getCheckIns()->checkIns;
                $openHouse->checkOuts= $openHouseRecord->getCheckOuts()->checkOuts;
                $oh['pastDue'][] = $openHouse;
            }
            else $oh['upcoming'][] = $openHouse;
            $oh['all'][] = $openHouse;
        }
        return $oh;
    }
}