<?php

namespace App\Http\Controllers;

use App\Library\Utilities\_LaravelTools;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Default page
     */
    protected $page = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    /**
     * Get corresponding view for route.
     * @param $page
     * @param array $data
     * @param bool $add
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($page)
    {
        $this->page = $page;
        $data       = [];
        $user       = new UserController();

        $properties = $user->getProperties();
        $openHouses = $user->getOpenHouses();
        $leads      = $user->getLeads();

        return view(_LaravelTools::addVersionToViewName($this->page.'.main'),[
            'page'          => $this->page,
            'properties'    => $properties,
            'openHouses'    => $openHouses,
            'leads'         => $leads,
            'data'          => $data,
        ]);
    }

    public function add($page)
    {
        $this->page = $page;
        return view(_LaravelTools::addVersionToViewName($this->page.'.add'),[

        ]);
    }

    public function details($page, $id)
    {
        $this->page = $page;
        $data       = [];
        switch ($this->page)
        {
            case 'clients':
                $client     = new ClientController();
                $data       = $client->details($id);
                break;
            case 'leads':
                $lead       = new LeadController();
                $data       = $lead->details($id)->record();
                break;
            case 'openHouses':
                $openHouse  = new OpenHouseController();
                $data       = $openHouse->details($id)->withProperty()->record();
                break;
            case 'properties':
                $property   = new PropertyController();
                $data       = $property->details($id)->record();
                break;
        }
        return view(_LaravelTools::addVersionToViewName($this->page.'.details'),[
            'data' => $data,
        ]);
    }
}
