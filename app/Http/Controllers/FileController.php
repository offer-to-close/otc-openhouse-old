<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/22/2019
 * Time: 1:25 PM
 */

namespace App\Http\Controllers;


use App\Library\Utilities\_Crypt;
use Illuminate\Support\Facades\Config;

class FileController extends Controller
{
    public function getFilestackSignature()
    {
        $date = new \DateTime('today +1000 days');
        $date = strtotime($date->format('Y-m-d H:i:s'));
        $policy = array(
            'call' => array(
                'pick',
                'read',
                'store',
                'convert'
            ),
            'expiry' => $date,
        );
        $jsonPolicy = json_encode($policy);
        $urlSafeBase64EncodedPolicy = _Crypt::base64url_encode($jsonPolicy);
        $signature = hash_hmac('sha256',$urlSafeBase64EncodedPolicy,Config::get('constants.FILESTACK_SECRET'));
        return response()->json([
            'policy' => $urlSafeBase64EncodedPolicy,
            'signature' => $signature,
        ]);
    }
}