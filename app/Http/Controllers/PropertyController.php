<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/15/2019
 * Time: 9:39 AM
 */

namespace App\Http\Controllers;


use App\Http\Requests\AddProperty;
use App\Library\Utilities\_LaravelTools;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Facades\Route;

class PropertyController extends Controller_Parent
{
    protected $property        = [];

    function  __construct()
    {
        $this->model = new Property();
        parent::__construct();
    }

    public function add(AddProperty $request)
    {
        $validated = $request->validated();
        $this->updateOrInsert($validated);
        return redirect()->route('get.page', ['name' => 'properties']);
    }

    /**
     * Deprecated. Use UserController@getProperties() instead.
     * @return mixed
     */
    public function deprecated_byUser()
    {
        $user = User::find(auth()->id());
        return $user->properties()->get();
    }

    /**
     * $this->record must not be empty for this to work, used in method chaining.
     * Example: $propertyControllerObject->details($id)->withOpenHouses()->record();
     * @return $this
     */
    public function withOpenHouses()
    {
        $property = Property::find($this->id);
        $openHouses = [];
        if($property)
        {
            $openHouses = $property->openHouses()->get();
        }
        $this->record->openHouses = OpenHouseController::openHousesByTimeInterval($openHouses);
        return $this;
    }

    /**
     * Controller_Parent Constructor Explanation.
     * ________________________________________________
     * Inside of classes that extend 'Controller_Parent' class, some child class methods may require the $id parameter
     * but appear to not use the $id parameter inside of the method. The way that those classes work is as follows:
     * Laravel automatically instantiates an object of the class when a method is used within that class, inside of
     * Controller_Parent child class constructors, the constructor assigns $this->model to be the model relevant to that
     * class. It also calls Controller_Parent constructor. Since it is not possible to inject the $id parameter directly
     * into Controller_Parent constructor, we look at the route and see if there is an 'id' parameter, if there is we
     * assign $this->id to it and fetch the relevant record so that Controller_Parent child classes can use the data.
     * This is why it is still necessary for methods in child classes that will be working with specific records to
     * have an $id parameter.
     *
     * Children classes of Controller_Parent that do not have any sort of id parameter are either to be used for method
     * chaining or are being used for post requests (adding new data, etc.)
     */
    public function viewOpenHouses($id)
    {
        return view(_LaravelTools::addVersionToViewName('openHouses.forProperty'),[
            'data' => $this->withOpenHouses()->record(),
        ]);
    }
}