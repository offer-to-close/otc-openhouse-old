<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 5/15/2019
 * Time: 2:41 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\AddClient;
use Illuminate\Support\Facades\Route;

class ClientController extends Controller
{
    protected $client = [];
    protected $id     = 0;

    public function __construct()
    {
        $this->id = Route::current()->parameter('id') ?? 0;
    }

    public function details($id)
    {
        //get the data from database or API

        //Temporary data for testing.
        $this->client = [
            'ID'                => $this->id,
            'Name'              => 'Bryan F',
            'PrimaryPhone'      => '818-855-4554',
            'SecondaryPhone'    => '818-233-2700',
            'PrimaryEmail'      => 'hi@email.com',
            'SecondaryEmail'    => 'bye@email.com',
            'PropertyVisited'   => '899 Not Cangas Again',
        ];

        return $this->client;
    }

    public function add(AddClient $request)
    {
        $validated = $request->validated();
        //$validated is the data, add it to the database.
    }
}