<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data               = $this->all();


        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'            => 'required',
            'last_name'             => 'required',
            'company_name'          => 'required',
            'phone'                 => 'required',
            'photo'                 => 'nullable',
            'email'                 => 'required|email:unique',
            'current_password'      => 'required',
            'password'              => 'nullable',
            'password_confirmation' => 'nullable|same:password'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required'                   => 'First Name is required.',
            'last_name.required'                    => 'Last Name is required.',
            'company_name.required'                 => 'Company Name is required.',
            'phone.required'                        => 'Phone Number is required.',
            'email.required'                        => 'Email is required.',
            'current_password.required'             => 'Password is required.',
            'password_confirmation.same'            => 'The new password and password confirmation must match.',
        ];
    }
}
