<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadSignOut extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data               = $this->all();


        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                    => 'required',
            'rating'                => 'required',
            'price_of_property'     => 'required',
            'interested_in_property'=> 'required',
            'feedback'              => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'id.required'                   => 'Please select a name.',
            'rating.required'               => 'Rating is required.',
            'price_of_property.required'    => 'Price assessment is required.',
            'interested_in_property.numeric'=> 'Interest assessment is required.',
        ];
    }
}
