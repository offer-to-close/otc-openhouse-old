<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadSignIn extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data               = $this->all();

        $data['phone'] = preg_replace('/[^0-9]/i', '', $data['phone']);
        $data['name'] = $data['first_name'].' '.$data['last_name'];

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'property_id'           => 'required',
            'open_house_id'         => 'required',
            'name'                  => 'required',
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'phone'                 => 'required|numeric',
            'email'                 => 'string',
            'looking_to_buy'        => 'required',
            'working_with_agent'    => 'required',
            'pre_approved_mortgage' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'property_id.required'          => 'Property is required.',
            'open_house_id'                 => 'Open House required.',
            'name.required'                 => 'First and Last name required.',
            'first_name.required'           => 'First Name is required.',
            'last_name.required'            => 'Last Name is required.',
            'phone.numeric'                 => 'Phone must be numeric.',
            'email.required'                => 'Email is required.',
            'looking_to_buy.numeric'        => 'This field is required.',
            'working_with_agent.required'   => 'This field is required.',
            'pre_approved_mortgage.required'=> 'This field is required.',
        ];
    }
}
