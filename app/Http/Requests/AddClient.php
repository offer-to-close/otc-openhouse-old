<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data               = $this->all();

        //you can manipulate data before validation here, see AddProperty for an example.

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'NameFirst'         => 'required',
            'NameLast'          => 'required',
            'PhonePrimary'      => 'required',
            'EmailPrimary'      => 'required',
            'PropertyVisited'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'NameFirst.required'        => 'First Name is required',
            'NameLast.required'         => 'Last Name is required.',
            'PhonePrimary.required'     => 'Phone Number is required.',
            'EmailPrimary.required'     => 'Email is required.',
            'PropertyVisited.required'  => 'Property Visited is required.',
        ];
    }
}
