<?php

namespace App\Http\Requests;

use App\Library\otc\AddressVerification;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;

class AddProperty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return TRUE;
    }

    /**
     * Manipulate the data before hand if you want.
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $data                   = $this->all();
        $squareFeet             = (int)str_replace(',','',$data['square_footage']);
        $data['square_footage'] = $squareFeet ? $squareFeet : '';
        $price                  = (double)str_replace(',','',$data['price']);
        $data['price']          = $price ? $price : '';

        if($data['agent'] == 'That\'s Me')
            $data['agent_name'] = auth()->user()->first_name.' '.auth()->user()->last_name;


        if (!$data['address'] || !$data['city'] || !$data['state'] || !$data['zip']) $address = $data['paddress'].' '.$data['paddress2'];
        else $address = $data['address'].', '.$data['city'].' '.$data['state'].' '.$data['zip'];

        $addressVerify = new AddressVerification();
        $addressList = $addressVerify->isValid($address);

        if($addressList)
        {
            $data['address'] = $addressList[0]['Street1'];
            $data['city']    = $addressList[0]['City'];
            $data['state']   = $addressList[0]['State'];
            $data['zip']     = $addressList[0]['Zip'];
        }
        else
        {
            $error = ValidationException::withMessages([
                'Address' => ['The address is not valid.'],
            ]);
            throw $error;
        }

        $this->getInputSource()->replace($data);
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'           => 'required|int',
            'address'           => 'required|string',
            'city'              => 'string',
            'state'             => 'string|max:2',
            'zip'               => 'string',
            'photo'             => 'nullable',
            'price'             => 'required|numeric',
            'square_footage'    => 'required|numeric',
            'type'              => 'required',
            'bedrooms'          => 'required',
            'bathrooms'         => 'required',
            'agent'             => 'required',
            'agent_name'        => 'required',
            'client_name'       => 'required|string',
            'status'            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'address.required'          => 'Address is required.',
            'price.required'            => 'Price is required.',
            'price.numeric'             => 'Price must be a number.',
            'square_footage.required'   => 'Square Feet is required.',
            'square_footage.numeric'    => 'Square Feet must be a number.',
            'type.required'             => 'Property Type is required.',
            'bedrooms.required'         => 'Number of Bedrooms is required.',
            'bathrooms.required'        => 'Number of Bathrooms is required.',
            'agent_name.required'       => 'The name of the agent is required.',
            'client_name.required'      => 'Client Name is required.',
        ];
    }
}
