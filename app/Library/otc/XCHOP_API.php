<?php


namespace App\Library\otc;


use Illuminate\Support\Facades\Config;

class XCHOP_API
{
    protected $url;
    protected $key;
    protected $headers;
    protected $payload;
    protected $response;

    public function __construct()
    {
        $this->url = Config::get('otc.XCHOP.url');
        $this->key = Config::get('otc.XCHOP.key');
        $this->headers = array(
            'Content-Type:application/json;charset=UTF-8',
            'Authorization:key='.$this->key,
        );
    }

    public function getURL()
    {
        return $this->url;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setHeaders($params = [])
    {
        $this->headers = $params;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setPayload($params = [])
    {
        $this->payload = json_encode($params);
        return $this;
    }

    public function getPayload($JSON = FALSE)
    {
        if($JSON) return $this->payload;
        return json_decode($this->payload);
    }

    /**
     * Call is the what API method being executed, for example
     * $call = 'create_user' which is the registration call.
     * @param $call
     * @param null $appendToURL
     * @param bool $isPOST
     * @param bool $hasReturnTransfer
     * @return $this
     */
    public function execute($call, $appendToURL = NULL, $isPOST = TRUE, $hasReturnTransfer = TRUE)
    {
        $curl = curl_init();
        if ($isPOST)
        {
            $options = array(
                CURLOPT_URL => $this->url . $call,
                CURLOPT_POST => $isPOST,
                CURLOPT_RETURNTRANSFER => $hasReturnTransfer,
                CURLOPT_HTTPHEADER => $this->headers,
                CURLOPT_POSTFIELDS => $this->payload,
            );
        }
        else
        {
            $options = array(
                CURLOPT_URL => $this->url . $call.'/'.$appendToURL,
                CURLOPT_RETURNTRANSFER => $hasReturnTransfer,
                CURLOPT_HTTPHEADER => $this->headers,
            );
        }
        curl_setopt_array($curl, $options);
        $this->response = curl_exec($curl);
        return $this;
    }

    public function getResponse($JSON = FALSE)
    {
        if($JSON) return $this->response;
        return json_decode($this->response);
    }

    public function isSuccess()
    {
        $response = json_decode($this->response);
        return $response->Response->status == 'success' ? TRUE:FALSE;
    }
}