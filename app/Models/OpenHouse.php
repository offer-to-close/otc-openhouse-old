<?php

namespace App\Models;


class OpenHouse extends Model_Parent
{
    //use SoftDeletes;
    protected $table='open_house';

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function leads()
    {
        return $this->hasMany(Lead::class);
    }
}
