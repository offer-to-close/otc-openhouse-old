<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'company_name',
        'photo',
        'email',
        'email_verified_at',
        'password',
        'status',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function properties()
    {
        return $this->hasMany(Property::class, 'user_id');
    }

    public function leads()
    {
        return $this->hasManyThrough(Lead::class, Property::class);
    }

    public function openHouses()
    {
        return $this->hasManyThrough(OpenHouse::class, Property::class);
    }
}
