<?php

namespace App\Models;


class Property extends Model_Parent
{
    //use SoftDeletes;
    public $table = 'property_details';

    public function openHouses()
    {
        return $this->hasMany(OpenHouse::class, 'property_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
