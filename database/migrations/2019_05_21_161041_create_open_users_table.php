<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->integer('open_house_id');
            $table->string('name');
            $table->bigInteger('phone');
            $table->string('email');
            $table->string('looking_to_buy');
            $table->string('working_with_agent');
            $table->string('pre_approved_mortgage');
            $table->string('rating')->nullable();
            $table->string('price_of_property')->nullable();
            $table->string('interested_in_property')->nullable();
            $table->mediumText('feedback')->nullable();
            $table->integer('status')->comment('1:signed in, 2: signed out');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_users');
    }
}
