@extends('auth.layouts.master')
@section('content')
<body id="register">
    <div class="container-fluid body">
        <!-- LOGO -->
        <a href="#" >
            <img src="{{asset('images/logo.png')}}" class="mt-2 ml-3">
        </a>
        <!-- Content -->
        <div class="container content mt-3 mb-4">
            <div class="row">
                <div class="col-sm-6" id="sign">
                    <center>
                        <p>Already have an account?</p>
                        <a href="{{route('login')}}"><button class="btn btn-outline-danger">SIGN IN</button></a>
                    </center>
                </div>
                <div class="col-sm-6 py-4" id="reg">
                    <form action="{{route('register')}}" method="POST">
                        <h4 class="text-center">REGISTER</h4>
                        <input type="hidden" value="{{csrf_token()}}" name="_token">
                        <input type="hidden" value="" name="request_id">
                        <div class="form-group row mb-1 mt-0" style="border:none;">
                            <div class="col-sm-6">
                                <label for="first_name" class=" col-form-label mb-0">First Name</label>
                                <input name="first_name" value="{{old('first_name') ?? NULL}}" type="text" class="form-control mt-0" id="fname" required>
                                @if($errors->has('first_name'))
                                    <span class="help-block">{{$errors->first('first_name')}}</span>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label for="last_name" class="col-form-label  mb-0">Last Name</label>
                                <input name="last_name" value="{{old('last_name') ?? NULL}}" type="text" class="form-control mt-0" id="lname" required>
                                @if($errors->has('last_name'))
                                    <span class="help-block">{{$errors->first('last_name')}}</span>
                                @endif
                            </div>
                        </div>

                        <div>
                            <label for="phone" class="mb-0">Phone Number</label>
                            <input maxlength="14" value="{{old('phone') ?? NULL}}" type="text" name="phone" class="form-control mb-1" required>
                            <span class="help-block hidden" id="phone_errors"></span>
                            @if($errors->has('phone'))
                                <span class="help-block">{{$errors->first('phone')}}</span>
                            @endif
                        </div>

                        <div>
                            <label for="company_name" class="mb-0">Company Name</label>
                            <input type="text" value="{{old('company_name') ?? NULL}}" name="company_name" class="form-control mb-1" required>
                            @if($errors->has('company_name'))
                                <span class="help-block">{{$errors->first('company_name')}}</span>
                            @endif
                        </div>

                        <div>
                            <label for="email" class="mb-0">Email Address</label>
                            <input type="email" value="{{old('email') ?? NULL}}" name="email" class="form-control mb-1" required>
                            @if($errors->has('email'))
                                <span class="help-block">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                        <div>
                            <label for="password" class="mb-0">Password</label>
                            <input type="password" name="password" class="form-control mb-1" required>
                            @if($errors->has('password'))
                                <span class="help-block">{{$errors->first('password')}}</span>
                            @endif
                        </div>
                        <div>
                            <label for="password-confirm" class="mb-0">Confirm Password</label>
                            <input id="password-confirm" type="password" class="form-control mb-1" name="password_confirmation" required>
                            @if($errors->has('password_confirmation'))
                                <span class="help-block">{{$errors->first('password_confirmation')}}</span>
                            @endif
                        </div>

                        <div class="form-check my-3">
                            <input class="form-check-input mt-2" type="checkbox" value="" id="terms" required>
                            <label class="form-check-label font-weight-bold" for="terms" style="color: black; font-size: 12px;">I agree to the <a href="#">Terms and Conditions</a></label>
                        </div>

                        <button type="button" class="btn btn-danger form-control" id="register_submit">REGISTER NOW</button>
                        <button type="submit" class="btn btn-danger form-control hidden" id="complete_registration"></button>
                    </form>
                </div>
            </div>
        </div>
        <footer>
            <p class="pt-0">&copy; OTC Open House 2019</p>
        </footer>
    </div>
    <script>
        $(document).ready(function () {
            $('input[name="phone"]').mask('(###)-###-####');
            let csrf             = '{{csrf_token()}}';

            $('#register_submit').click(function () {
                let html =
                    '<div class="row">' +
                    '   <div class="col-sm-12">' +
                    '       <p>' +
                    '           We have sent a verification code to the phone number provided.' +
                    '           Please enter the code below to complete registration.' +
                    '       </p>' +
                    '   </div>' +
                    '   <div class="col-sm-12">' +
                    '           <input class="col-6-sm m-auto" type="text" name="verification_code">' +
                    '           <span class="help-block hidden" id="nex_failure"></span>' +
                    '   </div>' +
                    '   <div class="col-sm-12 row" style="margin-top: 10px;">' +
                    '       <button style="margin-top: 10px !important;" class="col-xs-12 col-md-5 btn btn-danger form-control m-auto" type="button" id="submit_verification">Submit</button>' +
                    '       <button style="margin-top: 10px !important;" class="col-xs-12 col-sm-5 btn btn-danger form-control m-auto" type="button" id="cancel_verification" disabled>Cancel <span class="timer"></span></button>' +
                    '       <button style="margin-top: 10px !important;" class="col-xs-12 col-sm-6 btn btn-danger form-control m-auto hidden" type="button" id="resend_verification" disabled>Resend Code <span class="timer"></span></button>' +
                    '   </div>' +
                    '</div>';
                Swal2.fire({
                    html: html,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        let verifyNumberURL = '{{route('nexmo.verifyNumber')}}';
                        let phone           = $('input[name="phone"]').val();
                        let codeFailure     = $('#nex_failure');

                        let timer = 30;
                        let cancelResendText = $('#cancel_verification .timer, #resend_verification .timer');
                        setInterval(function () {
                            if (timer > 0)
                            {
                                timer = timer - 1;
                                cancelResendText.text(timer);
                            }
                            if(timer <= 0)
                            {
                                cancelResendText.text('');
                                $('#cancel_verification, #resend_verification').attr('disabled', false);
                                $('#resend_verification').removeClass('hidden');
                            }
                        },1000);


                        $.ajax({
                            url: verifyNumberURL,
                            method: 'POST',
                            data: {
                                phone: phone,
                                _token: csrf,

                            },
                            success: function (r) {
                                console.log(r);
                                if(r.status == 'fail')
                                {
                                    let errors = $('#phone_errors');
                                    errors.text(r.message);
                                    errors.removeClass('hidden');
                                    Swal2.close();
                                }
                                if(r.status == 'success')
                                {
                                    $('input[name="request_id"]').val(r.data.request_id);
                                }
                            },
                            error: function (err) {
                                console.log(err);
                            },
                        });

                        $('#submit_verification').click(function () {
                            let verification    = $('input[name="verification_code"]').val();
                            let request_id      = $('input[name="request_id"]').val() || '{{session('request_id')}}';
                            let verifyURL       = '{{route('nexmo.verifyCode')}}';

                            console.log({
                                verification: verification,
                                request_id: request_id,
                            });

                            $.ajax({
                                url: verifyURL,
                                method: 'POST',
                                data: {
                                    verification_code: verification,
                                    request_id: request_id,
                                    _token: csrf,

                                },
                                success: function (r) {
                                    console.log(r);
                                    if(r.status == 'fail')
                                    {
                                        if (r.status.message == 'The code provided does not match the expected value')
                                        {
                                            codeFailure.text(r.status.message);
                                            codeFailure.removeClass('hidden');
                                        }
                                    }
                                    else if (r.status == 'success')
                                    {
                                        Swal2.fire({
                                            text: 'Completing registration...',
                                            showConfirmButton: false,
                                            allowOutsideClick: false,
                                        });
                                        Swal2.showLoading();
                                        $('#complete_registration').trigger('click');
                                    }
                                    else
                                    {
                                        Swal2.fire({
                                            title: 'Error',
                                            type: 'error',
                                            text: 'An unknown error has occurred, please try again.',
                                        });
                                    }

                                },
                                error: function (err) {
                                    console.log(err);
                                },
                            });
                        });

                        $('#cancel_verification').click(function () {
                            let request_id       = $('input[name="request_id"]').val() || '{{session('request_id')}}';
                            let cancelURL        = '{{route('nexmo.cancel')}}';

                            $.ajax({
                                url: cancelURL,
                                method: 'POST',
                                data: {
                                    request_id: request_id,
                                    _token: csrf,

                                },
                                success: function (r) {
                                    console.log(r);
                                    if(r.status == 'success')
                                    {
                                        if (r.data.status == 0)
                                        {
                                            Swal2.close();
                                        }
                                    }
                                    else if(r.status == 'fail')
                                    {
                                        codeFailure.text(r.message);
                                        codeFailure.removeClass('hidden');
                                    }
                                    else
                                    {
                                        Swal2.fire({
                                           title: 'Error',
                                           type: 'error',
                                           text: 'An unknown error has occurred.',
                                        });
                                    }
                                },
                                error: function (err) {
                                    console.log(err);
                                },
                            });
                        });

                        $('#resend_verification').click(function () {
                            let request_id  = $('input[name="request_id"]').val() || '{{session('request_id')}}';
                            let phone       = $('input[name="phone"]').val();
                            let resendURL   = '{{route('nexmo.resend')}}';

                            $.ajax({
                                url: resendURL,
                                method: 'POST',
                                data: {
                                    request_id: request_id,
                                    phone: phone,
                                    _token: csrf,

                                },
                                success: function (r) {
                                    if (r.status == 'success')
                                    {
                                        $('input[name="request_id"]').val(r.request_id);
                                    }
                                    else if(r.status == 'fail')
                                    {
                                        codeFailure.text(r.message);
                                        codeFailure.removeClass('hidden');
                                    }
                                    else
                                    {
                                        Swal2.fire({
                                            title: 'Error',
                                            type: 'error',
                                            text: 'An unknown error has occurred.',
                                        });
                                    }
                                },
                                error: function (err) {
                                    console.log(err);
                                },
                            });

                        });

                    },
                });
            });
        });
    </script>
</body>
@endsection