<!DOCTYPE html>
<html>
<head>
    <title>Home Screen</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap_v4.3.1.css')}}">

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/app_1a.css')}}">
    @yield('custom_css')

    <!-- Scripts -->
    <script src="{{asset('js/jquery_v3.3.1.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

    <!-- Fonts CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
          crossorigin="anonymous">
</head>
@yield('content')
</html>