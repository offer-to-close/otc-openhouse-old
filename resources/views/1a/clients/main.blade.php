@extends('1a.layouts.master')
@section('content')
<body id="my-clients" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row py-2 content-body px-0 mb-0">
            <div class="col-sm-12">
                <h2 class="text-center py-3">View Clients</h2>

                <div class="row px-5 mb-2" id="cards">
                    <?php
                        $clients = [
                            [
                                'name'          => 'Mckay Kalani',
                                'email'         => 'hi@email.com',
                                'phone'         => '818-818-8888',
                                'streetAddress' => '5222 Cangas Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Some Person',
                                'email'         => 'bye@email.com',
                                'phone'         => '818-818-8888',
                                'streetAddress' => '9999 Cangas Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ],
                            [
                                'name'          => 'Wow Another Person',
                                'email'         => 'hello@email.com',
                                'phone'         => '818-818-2222',
                                'streetAddress' => '5222 New York Drive',
                                'details'       => route('get.page.details', ['name' => 'clients', 'id' => 1])
                            ]

                        ]
                    ?>
                    @foreach($clients as $client)
                        @if($loop->index < 5)
                            <div class="client-card col-sm-4 mb-2">
                                <div class="card">

                                    <div class="card-body pt-3 pb-2 pl-3">
                                        <h4 class="card-title" align="center">{{$client['name']}}</h4>
                                        <p class="card-text mb-2">
                                            <a href="#"><img src="{{asset('images/icon-address-1.png')}}">{{$client['streetAddress']}}</a>
                                        </p>
                                        <hr class="mt-0 mb-1">
                                        <p class="card-text mb-0">
                                            <a href="#"><img src="{{asset('images/icon-email-1.png')}}" style="height: 12px;">{{$client['email']}}</a>
                                        </p>
                                    </div>
                                    <div class="card-footer pl-3">
                                        <p class="card-text">
                                            <a href="{{$client['details']}}" class="stretched-link"><img src="{{asset('images/icon-phone-1.png')}}">  {{$client['phone']}}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="client-card col-sm-4 mb-2 hidden">
                                <div class="card">

                                    <div class="card-body pt-3 pb-2 pl-3">
                                        <h4 class="card-title" align="center">{{$client['name']}}</h4>
                                        <p class="card-text mb-2">
                                            <a href="#"><img src="{{asset('images/icon-address-1.png')}}">{{$client['streetAddress']}}</a>
                                        </p>
                                        <hr class="mt-0 mb-1">
                                        <p class="card-text mb-0">
                                            <a href="#"><img src="{{asset('images/icon-email-1.png')}}" style="height: 12px;">{{$client['email']}}</a>
                                        </p>
                                    </div>
                                    <div class="card-footer pl-3">
                                        <p class="card-text">
                                            <a href="{{$client['details']}}" class="stretched-link"><img src="{{asset('images/icon-phone-1.png')}}">  {{$client['phone']}}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="col-sm-4 mb-2 pb-5">
                        <center>
                            <a href="{{route('get.page.add', ['name' => 'clients'])}}">
                                <button class="btn"><i class="fas fa-plus"></i> Add Client</button>
                            </a>
                        </center>
                    </div>
                </div>
                <hr class="mb-2" style="border:1px solid #D93149;">

                <center><a id="more" class="view-more">VIEW MORE CLIENTS</a></center>

            </div>

        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {
            let moreElements = new showMoreElements('client-card',5);
            $('#more').click(function () {
                moreElements.viewMore(6);
            });
        });
    </script>
</body>
@endsection