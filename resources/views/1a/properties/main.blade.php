@extends('1a.layouts.master')
@section('content')
<body id="my-properties" class="main-layout">
    <div class="container-fluid content p-2">
        <div class="row py-2 content-body" style="margin-bottom: 20px;">
            <div class="col-sm-12">
                <h2 class="text-center py-3">My Properties</h2>
                <div class="row px-5 mb-2" id="cards">

                    @foreach($properties as $property)
                        @if($loop->index < 5) <div class="property-card col-sm-4 mb-2">
                        @else <div class="property-card col-sm-4 mb-2 hidden">
                        @endif
                                <div class="card">
                                    @if($property->photo)
                                        <img src="{{$property->photo}}" class="card-img-top img-responsive">
                                    @else
                                        <img src="{{asset('images/image_placeholder.png')}}" class="card-img-top img-responsive">
                                    @endif
                                    <div class="card-body pl-2 py-2" >
                                        <h5 class="card-title">{{$property->address ?? NULL}}</h5>
                                        <img src="{{asset('images/icon-client.png')}}" class="mx-0 client">
                                        <p class="card-text">
                                            <a href="{{route('get.page.details', ['name' => 'properties', 'id' => $property->id])}}" class="stretched-link"> <b>Client:</b> {{$property->client_name}}</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                    @endforeach

                    <!-- todo add a property -->
                    <div class="col-sm-4 mb-2 pb-5">
                        <center>
                            <a href="{{route('get.page.add', ['name' => 'properties'])}}">
                                <button class="btn"><i class="fas fa-plus"></i> Add Property</button>
                            </a>
                        </center>
                    </div>
                </div>


                <hr class="mb-2" style="border:1px solid #D93149;">

                <center><a id="more" class="view-more">VIEW MORE PROPERTIES</a></center>
            </div>
        </div>
        <p class="mb-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {
            let moreElements = new showMoreElements('property-card',5);
            $('#more').click(function () {
                moreElements.viewMore(6);
            });
        });
    </script>
</body>
@endsection