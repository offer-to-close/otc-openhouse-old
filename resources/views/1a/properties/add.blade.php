@extends('1a.layouts.master')
@section('content')
<body id="add-new-property" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row py-0 content-body">
            <div class="col-sm-6 slider py-3" style="background: url('{{asset('images/placeholder_384x540.jpg')}}');">
                <center>
                    <button class="btn btn-outline-danger" id="upload">Upload Photo</button><br>
                    <div id="property_image_frame" class="hidden">
                        <img class="img-responsive property_image" src="">
                    </div>
                </center>
            </div>
            <div class="col-sm-6 details">
                <h3 class="text-center mb-4">Add Property</h3>

                <form action="{{route('property.add')}}" method="POST">
                    <input type="hidden" value="{{auth()->id()}}" name="user_id">
                    <input type="hidden" name="address">
                    <input type="hidden" name="city">
                    <input type="hidden" name="state">
                    <input type="hidden" name="zip">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="photo" value="">
                    <input type="hidden" name="status" value="1">
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-p-address.png')}}" class="img-responsive">
                            <label for="paddress">Property Address</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="paddress" id="paddress"
                                   class="form-control" placeholder="" value="{{old('address') ?? NULL}}">
                        </div>
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6">
                            <input type="text" name="paddress2" id="paddress2"
                                   class="form-control add-input-2" placeholder=""
                                   value="{{old('city')??NULL.' '.old('state')??NULL.' '.old('zip')??NULL}}">
                            @if($errors->has('Address'))
                                <span class="help-block">{{$errors->first('Address')}}</span>
                            @endif
                            @if($errors->has('address'))
                                <span class="help-block">{{$errors->first('address')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-l-price.png')}}" class="img-responsive">
                            <label for="lprice">List Price</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="price" id="lprice" class="form-control"
                                   value="{{old('price') ?? NULL}}">
                            @if($errors->has('price'))
                                <span class="help-block">{{$errors->first('price')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-p-type.png')}}" class="img-responsive">
                            <label for="ptype">Property Type</label>
                        </div>
                        <?php
                            $propertyTypes = config('constants.propertyTypes');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="type">
                                @foreach($propertyTypes as $value => $display)
                                    <option value="{{$value}}">{{$display}}</option>
                                @endforeach
                            </select>
                            @if($errors->has('type'))
                                <span class="help-block">{{$errors->first('type')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-sq-foot.png')}}" class="img-responsive">
                            <label for="sqfoot">Square Footage</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="square_footage" id="sqfoot" class="form-control"
                                   value="{{old('square_footage') ?? NULL}}">
                            @if($errors->has('square_footage'))
                                <span class="help-block">{{$errors->first('square_footage')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-rooms.png')}}" class="img-responsive">
                            <label for="brooms"># of Bedrooms</label>
                        </div>
                        <?php
                        $bedroomOptions = config('constants.bedroomOptions');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="bedrooms">
                                @foreach($bedroomOptions as $value => $display)
                                    @if($value)
                                        @if($value == old('bedrooms'))
                                            <option value="{{$value}}" selected>{{$display}}</option>
                                        @else
                                            <option value="{{$value}}">{{$display}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('bedrooms'))
                                <span class="help-block">{{$errors->first('bedrooms')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-baths.png')}}" class="img-responsive">
                            <label for="bathrooms"># of Bathrooms</label>
                        </div>
                        <?php
                        $bathroomOptions = config('constants.bathroomOptions');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="bathrooms">
                                @foreach($bathroomOptions as $value => $display)
                                    @if($value == old('bathrooms'))
                                        <option value="{{$value}}" selected>{{$display}}</option>
                                    @else
                                        <option value="{{$value}}">{{$display}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if($errors->has('bathrooms'))
                                <span class="help-block">{{$errors->first('bathrooms')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="agent">Agent/s</label>
                        </div>
                        <div class="col-sm-6">
                            <select class="form-control" name="agent">
                                @if(!old('agent'))
                                    <option value="That's Me">That's Me</option>
                                    <option value="Someone Else">Someone Else</option>
                                @elseif(old('agent') == 'That\'s Me')
                                    <option value="That's Me" selected>That's Me</option>
                                    <option value="Someone Else">Someone Else</option>
                                @else
                                    <option value="That's Me">That's Me</option>
                                    <option value="Someone Else" selected>Someone Else</option>
                                @endif
                            </select>
                            @if($errors->has('agent_name'))
                                <span class="help-block">{{$errors->first('agent_name')}}</span>
                            @endif
                        </div>
                    </div>

                    @if(old('agent_name'))<div class="form-group row" id="agent_name">
                    @else <div class="form-group row hidden" id="agent_name">
                    @endif
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="agent">Agent Name</label>
                        </div>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" name="agent_name"
                                   value="{{old('agent_name') ?? NULL}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="client_name">Client Name</label>
                        </div>
                        <div class="col-sm-6">
                            <input class="form-control" type="text" name="client_name" value="{{old('client_name') ?? NULL}}">
                            @if($errors->has('client_name'))
                                <span class="help-block">{{$errors->first('client_name')}}</span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mt-4">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger form-control">Add Property</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
    <script>

        $(document).ready(function () {
            $('#lprice, #sqfoot').keyup(function () {
                $(this).val(currencyFormat($(this).val()));
            });

            $('select[name="agent"]').change(function () {
                if($(this).val() == 'Someone Else') $('#agent_name').removeClass('hidden');
            });
        });

        function initAutocomplete()
        {
            /**
             * Create the auto complete object, restricting the search to geographical
             * location types. This auto complete is attached to the top address bar inside of transaction home.
             */

            let fillInAddressSwitch = function(){
                fillInAddress(0);
            };
            autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('paddress')),
                {types: ['geocode']});
            autocomplete.addListener('place_changed', fillInAddressSwitch);

            fillInAddressSwitch = function(){
                fillInAddress(1);
            };
            autocomplete2 = new google.maps.places.Autocomplete(
                (document.getElementById('paddress2')),
                {types: ['geocode']});
            autocomplete2.addListener('place_changed', fillInAddressSwitch);
        }

        function fillInAddress(num)
        {
            // Get the place details from the auto complete object.

            function formatAddress(object)
            {
                let streetAddressInput  = $('#paddress');
                let cityAndZipInput     = $('#paddress2');
                streetAddressInput.val('');
                cityAndZipInput.val('');
                var place           = object.getPlace();
                let streetAddress   = place.name;
                let city            = place.address_components[3].long_name;
                let state           = place.address_components[5].short_name;
                let zip             = place.address_components[7].short_name;
                streetAddressInput.val(streetAddress);
                cityAndZipInput.val(city+', '+state+' '+zip);
                $('input[name="address"]').val(streetAddress);
                $('input[name="city"]').val(city);
                $('input[name="state"]').val(state);
                $('input[name="zip"]').val(zip);
            }

            if (!num)
            {
                try {
                    formatAddress(autocomplete, 'paddress');
                } catch (e) {
                    //this try to avoid crashing future code if this object is empty.
                }
            }
            else if (num)
            {
                try {
                    formatAddress(autocomplete2, 'paddress2');
                } catch (e) {
                    //this try to avoid crashing future code if this object is empty.
                }
            }
        }
    </script>
    <script>
        $(document).ready(function () {
            $('#phone').mask('(###)-###-####');
            $('#upload').click(function () {
                let filestackSignatureRoute = '{{route('filestack.signature')}}';
                $.ajax({
                    url: filestackSignatureRoute,
                    method: 'GET',
                    success: function (r) {
                        var policy      = r.policy;
                        var signature   = r.signature;
                        const security  = {
                            policy: policy,
                            signature: signature,
                        };

                        const clientOptions = {security};
                        const api_key = 'AzZlFYkLVRHKkQDAtOrYpz';
                        const client = filestack.init(api_key, clientOptions);
                        const options = {
                            maxFiles: 1,
                            accept: 'image/*',
                            uploadInBackground: false,
                            fromSources: [
                                'local_file_system',
                                'url',
                                'googledrive',
                                'dropbox',
                                'gmail',
                                'onedrive',
                                'onedriveforbusiness',
                                'clouddrive',
                                'box',
                            ],
                            onUploadDone: (res) => {

                                if(res.filesFailed.length > 0)
                                {
                                    Swal2.fire({
                                        title:   'Upload failed',
                                        text:    'Reference code: file-upload-002',
                                    });
                                }
                                if(res.filesUploaded.length > 0)
                                {
                                    let uploadedFile = res.filesUploaded[0];
                                    let urlParts = uploadedFile.url.split('/');
                                    let handle = urlParts[3];
                                    let processURL =
                                        urlParts[0]+'//'+
                                        urlParts[2]+'/'+
                                        'security=p:'+ policy+
                                        ',s:'+signature+
                                        '/resize=w:384,h:538,fit:crop/'+
                                        handle;
                                    let url = uploadedFile.url + '?policy='+policy+'&signature='+signature;
                                    $('input[name="photo"]').val(url);
                                    let propertyImage = $('.property_image');
                                    propertyImage.attr('src',processURL);
                                    $('#upload').remove();
                                    $('#property_image_frame').removeClass('hidden');
                                    if(propertyImage.hasClass('hidden')) propertyImage.removeClass('hidden');

                                } //end of if uploadedFiles == 1
                            }, //end of onUploadDone
                        }; //end options

                        client.picker(options).open();
                    },
                    error: function () {
                        Swal2.fire({
                            title: 'Error',
                            type: 'error',
                            text: 'An unknown error has occurred.',
                        });
                    },
                });
            });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4J1w4-TUMxlpNYl_NZDl6_Dg2cPhrKS8&libraries=places&callback=initAutocomplete"
            async defer>
    </script>
</body>
@endsection