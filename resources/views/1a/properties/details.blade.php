@extends('1a.layouts.master')
@section('content')
<body id="property-details" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row p-0 content-body">
            <div class="col-sm-6 px-0" id="carousel">

                <div id="img-slider" class="carousel slide mx-0" data-ride="carousel" data-interval="false" data-keyboard="true" data-pause="false">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#img-slider" data-slide-to="0" class="active"></li>
                        <!-- <li data-target="#img-slider" data-slide-to="1"></li>
                        <li data-target="#img-slider" data-slide-to="2"></li>
                        <li data-target="#img-slider" data-slide-to="3"></li> -->
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <!-- todo replace this with actual property images -->
                            @if($data['photo'])
                                <img src="{{$data['photo']}}" class="img-responsive image_upload" style="width: 100%;">
                            @else
                                <img src="{{asset('images/image_placeholder.png')}}" class="img-responsive image_upload" style="width: 100%;">
                            @endif
                        </div>
                        <!--
                        <div class="carousel-item">
                            <img src="images/slide-2.jpg" class="img-responsive" style="height: 500px;">
                        </div>
                        <div class="carousel-item">
                            <img src="images/slide-3.jpg" class="img-responsive" style="height: 500px;">
                        </div>
                        <div class="carousel-item">
                            <img src="images/slide-4.jpg" class="img-responsive" style="height: 500px;">
                        </div> -->
                    </div>

                    <!-- Left and right controls -->
                    <!--
                    <a class="carousel-control-prev" href="#img-slider" data-slide="prev">
                        <i class="fas fa-chevron-left"></i>
                    </a>
                    <a class="carousel-control-next" href="#img-slider" data-slide="next">
                        <i class="fas fa-chevron-right"></i>
                    </a>
                    -->
                </div>
            </div>
            <div class="col-sm-6 details">
                <center>
                    <input type="text" name="property-address" value="{{$data['address']}}" id="property-address" class="form-control py-0">
                </center>

                <form>


                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-l-price.png')}}" class="img-responsive">
                            <label for="lprice">List Price</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="price" id="lprice" class="form-control"
                                   value="${{number_format($data['price'],0)}}" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-p-type.png')}}" class="img-responsive">
                            <label for="ptype">Property Type</label>
                        </div>
                        <?php
                            $propertyTypes = config('constants.propertyTypes');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="type" disabled>
                                @foreach($propertyTypes as $value => $display)
                                    @if($value == $data['type'])
                                        <option value="{{$value}}" selected>{{$display}}</option>
                                    @else
                                        <option value="{{$value}}">{{$display}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-sq-foot.png')}}" class="img-responsive">
                            <label for="sqfoot">Square Footage</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="square_footage" id="sqfoot" class="form-control"
                                   value="{{number_format($data['square_footage'])}} sq ft" disabled>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-rooms.png')}}" class="img-responsive">
                            <label for="brooms"># of Bedrooms</label>
                        </div>
                        <div class="col-sm-6">
                            <?php
                                $bedroomOptions = config('constants.bedroomOptions');
                            ?>
                            <select class="form-control" name="bedrooms" disabled>
                                @foreach($bedroomOptions as $value => $display)
                                    @if($value)
                                        @if($value == $data['bedrooms'])
                                            <option selected>{{$display}}</option>
                                        @else
                                            <option>{{$display}}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-baths.png')}}" class="img-responsive">
                            <label for="bathrooms"># of Bathrooms</label>
                        </div>
                        <?php
                            $bathroomOptions = config('constants.bathroomOptions');
                        ?>
                        <div class="col-sm-6">
                            <select class="form-control" name="bathrooms" disabled>
                                @foreach($bathroomOptions as $value => $display)
                                    @if($value == $data['bathrooms'])
                                        <option selected>{{$display}}</option>
                                    @else
                                        <option>{{$display}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="agent">Agent/s</label>
                        </div>
                        <div class="col-sm-6">
                            <select class="form-control" name="agent" disabled>
                                @if($data['agent'] == 'That\'s Me')
                                    <option>That's Me</option>
                                @else
                                    <option>Someone Else</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6">
                            <img src="{{asset('images/icon-agents.png')}}" class="img-responsive">
                            <label for="agent">Agent Name</label>
                        </div>
                        <div class="col-sm-6">
                            <input type="text" name="agent_name" id="agent_name" class="form-control"
                                   value="{{$data['agent_name']}}" disabled>
                        </div>
                    </div>

                    <div class="form-group row mt-4" style="border:none;">
                        <div class="col-sm-6 pr-2">
                            <a href="{{route('property.openHouses', ['id' => $data['id']])}}">
                                <button type="button" class="btn btn-danger form-control">View Open Houses</button>
                            </a>
                        </div>
                        <div class="col-sm-6 pl-2">
                            <button type="button" class="btn btn-outline-danger form-control" id="reports">Reports</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <p class="my-3 footer">&copy; OTC Open House 2019</p>
    </div>
</body>
@endsection