@extends('1a.layouts.master')
@section('content')
    <body id="open-houses" class="main-layout">
    <div class="container-fluid content p-3 mt-5">
        <div class="row content-body">
            <div class="col-sm-12">
                <h2 class="text-center pb-4">Open Houses for {{$data['address']}}</h2>

                <div class="row upcoming-events">
                    <div class="col-sm-12 pb-3">
                        <h6>Upcoming Open Houses</h6>
                    </div>
                    <?php
                    $upcomingOpenHouses = $data['openHouses']['upcoming'];
                    ?>
                    @foreach($upcomingOpenHouses as $openHouse)
                        @if($loop->index < 3)
                            <div class="upcoming-openHouse-card col-sm-4 mb-3">
                                <div class="card">
                                    <div class="card-header text-center">
                                        {{$data['address']}}
                                    </div>
                                    <div class="card-body py-2 date">
                                        <p class="card-text text-center">
                                            {{date('l, F Y', strtotime($openHouse['date']))}}
                                        </p>
                                    </div>
                                    <div class="card-body py-2 time">
                                        <p class="card-text  text-center font-weight-bold">
                                            <i class="far fa-clock"></i> {{date('g:iA', strtotime($openHouse['start_time']))}}-{{date('g:iA', strtotime($openHouse['end_time']))}}
                                        </p>
                                    </div>
                                    <div class="card-body activity text-center">
                                        <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link">
                                            No activity yet
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row past-events">
                    <div class="col-sm-12 pb-3">
                        <h6>Past Open Houses</h6>
                    </div>
                    <?php
                    $pastOpenHouses = $data['openHouses']['pastDue'];
                    ?>
                    @foreach($pastOpenHouses as $openHouse)
                        @if($loop->index < 3) <div class="past-openHouse-card col-sm-4 mb-3">
                            @else <div class="past-openHouse-card col-sm-4 mb-3 hidden">
                                @endif
                                <div class="card">
                                    <div class="card-header text-center">
                                        {{$data['address']}}
                                    </div>
                                    <div class="card-body py-2 date">
                                        <p class="card-text text-center">
                                            {{date('l, F Y', strtotime($openHouse['date']))}}
                                        </p>
                                    </div>
                                    <div class="card-body py-2 time">
                                        <p class="card-text  text-center font-weight-bold">
                                            <i class="far fa-clock"></i> {{date('g:iA', strtotime($openHouse['start_time']))}}-{{date('g:iA', strtotime($openHouse['end_time']))}}
                                        </p>
                                    </div>
                                    <div class="card-body activity text-center px-2 pt-3 pb-0">
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-check-in.png')}}"><br>
                                            @if(isServerLocal())
                                                <span class="num">{{$openHouse['checkIns']}}</span><br>
                                            @else
                                                <span class="num">{{count($openHouse->checkIns)}}</span><br>
                                            @endif
                                            <span class="name">Check-ins</span>
                                        </p>
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-check-out.png')}}"><br>
                                            @if(isServerLocal())
                                                <span class="num">{{$openHouse['checkOuts']}}</span><br>
                                            @else
                                                <span class="num">{{count($openHouse->checkOuts)}}</span><br>
                                            @endif
                                            <span class="name">Check-outs</span>
                                        </p>
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-text-msg-2.png')}}" style="width: 20px;"><br>
                                            @if(isServerLocal())
                                                <span class="num">{{$openHouse['comments']}}</span><br>
                                            @else
                                                <span class="num">{{count($openHouse->comments)}}</span><br>
                                            @endif
                                            <span class="name">Comments</span>
                                        </p>
                                        <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link"></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>

                        <div class="row choose mt-2">
                            <div class="col-sm-6 mb-3">
                                <button type="button" class="btn form-control"><img src="{{asset('images/icon-leads-2.png')}}"> Choose Events to View Leads</button>
                            </div>
                            <div class="col-sm-6 mb-3">
                                <button type="button" class="btn form-control"><img src="{{asset('images/icon-report-3.png')}}"> Choose Events to Generate Reports</button>
                            </div>
                        </div>

                        <hr class="mb-2" style="">

                        <center><a id="more" class="view-more">VIEW MORE OPEN HOUSES</a></center>

                </div>
            </div>
            <p class="my-3 footer">&COPY; OTC Open House 2019</p>
        </div>
        <script>
            $(document).ready(function () {
                let moreElements = new showMoreElements('past-openHouse-card', 3);
                $('#more').click(function () {
                    moreElements.viewMore(3);
                });
            });
        </script>
    </body>
@endsection