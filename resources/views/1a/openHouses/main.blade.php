@extends('1a.layouts.master')
@section('content')
<body id="open-houses" class="main-layout">
    <div class="container-fluid content p-3 mt-5">
        <div class="row content-body"  style="margin-top: -295px;">
            <div class="col-sm-12">
                <h2 class="text-center pb-4">Open Houses</h2>

                <div class="row upcoming-events">
                    <div class="col-sm-12 pb-3">
                        <h6>Upcoming Open Houses</h6>
                    </div>
                    <?php
                        $upcomingOpenHouses = $openHouses['upcoming'];
                    ?>
                    @foreach($upcomingOpenHouses as $openHouse)
                        @if($loop->index < 3)
                            <div class="upcoming-openHouse-card col-sm-4 mb-3">
                                <div class="card">
                                    <div class="card-header text-center">
                                        {{$openHouse['address']}}
                                    </div>
                                    <div class="card-body py-2 date">
                                        <p class="card-text text-center">
                                            {{date('l, F Y', strtotime($openHouse['date']))}}
                                        </p>
                                    </div>
                                    <div class="card-body py-2 time">
                                        <p class="card-text  text-center font-weight-bold">
                                            <i class="far fa-clock"></i> {{date('g:iA', strtotime($openHouse['start_time']))}}-{{date('g:iA', strtotime($openHouse['end_time']))}}
                                        </p>
                                    </div>
                                    <div class="card-body activity text-center">
                                        <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link">
                                            No activity yet
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>

                <div class="row add mb-3 mt-1">
                    <div class="col-sm-12">
                        <button
                                type="button"
                                class="btn btn-outline-danger form-control"
                                data-toggle="collapse" href="#addOH" role="button" aria-expanded="false" aria-controls="addOH">
                            <i class="fas fa-plus" style="border:1px solid #D93149; border-radius: 360px; padding: 6px; margin-right: 6px; font-size: 10px;"></i> Add an Open House
                        </button>
                        <div class="row add mb-3 mt-1 collapse" id="addOH">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body px-4">
                                        <form action="{{route('openHouse.add')}}" method="POST">
                                            <div class="form-group row date">
                                                <input type="hidden" value="{{csrf_token()}}" name="_token">
                                                <div class="col-sm-6 px-3 py-1 mb-2">
                                                    <label for="date">Open House Date</label>
                                                    <input name="date" type="date" value="" id="date" required>
                                                </div>

                                                <div class="col-sm-6 py-1 mb-2" style="border-radius: 4px; background-color: #eeeeee;">
                                                    <div class="col-sm-12">
                                                        <label for="stime" >Start Time</label>
                                                        <input name="start_time" type="time" id="stime" value="" required>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <label for="etime">End Time</label>
                                                        <input name="end_time" type="time" id="etime" value="" required>
                                                    </div>
                                                    @if($errors->has('start_time'))
                                                        <span class="help-block">{{$errors->first('start_time')}}</span>
                                                    @endif
                                                    @if($errors->has('end_time'))
                                                        <span class="help-block">{{$errors->first('end_time')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6">
                                                    <label for="housetype" id="left" cl>Open House Type</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="form-control" id="housetype" name="type" required>
                                                        <option>Consumer Focused</option>
                                                        <option>Broker Focused</option>
                                                    </select>
                                                    @if($errors->has('type'))
                                                        <span class="help-block">{{$errors->first('type')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-6">
                                                    <label for="paddress"  id="left">Property Address</label>
                                                </div>
                                                <div class="col-sm-6">
                                                    <select class="form-control" id="paddress" name="property_id">
                                                        @foreach($properties as $property)
                                                            <option value="{{$property->id}}">{{$property->address}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has('property_id'))
                                                        <span class="help-block">{{$errors->first('property_id')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-danger"  id="left">Save Changes</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row past-events">
                    <div class="col-sm-12 pb-3">
                        <h6>Past Open Houses</h6>
                    </div>
                    <?php
                        $pastOpenHouses = $openHouses['pastDue'];
                    ?>
                    @foreach($pastOpenHouses as $openHouse)
                        @if($loop->index < 3) <div class="past-openHouse-card col-sm-4 mb-3">
                        @else <div class="past-openHouse-card col-sm-4 mb-3 hidden">
                        @endif
                                <div class="card">
                                    <div class="card-header text-center">
                                        {{$openHouse['address']}}
                                    </div>
                                    <div class="card-body py-2 date">
                                        <p class="card-text text-center">
                                            {{date('l, F Y', strtotime($openHouse['date']))}}
                                        </p>
                                    </div>
                                    <div class="card-body py-2 time">
                                        <p class="card-text  text-center font-weight-bold">
                                            <i class="far fa-clock"></i> {{date('g:iA', strtotime($openHouse['start_time']))}}-{{date('g:iA', strtotime($openHouse['end_time']))}}
                                        </p>
                                    </div>
                                    <div class="card-body activity text-center px-2 pt-3 pb-0">
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-check-in.png')}}"><br>
                                            <span class="num">{{count($openHouse->checkIns)}}</span><br>
                                            <span class="name">Check-ins</span>
                                        </p>
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-check-out.png')}}"><br>
                                            <span class="num">{{count($openHouse->checkOuts)}}</span><br>
                                            <span class="name">Check-outs</span>
                                        </p>
                                        <p class="card-text">
                                            <img src="{{asset('images/icon-text-msg-2.png')}}" style="width: 20px;"><br>
                                            <span class="num">{{count($openHouse->comments)}}</span><br>
                                            <span class="name">Comments</span>
                                        </p>
                                        <a href="{{route('get.page.details', ['name' => 'openHouses', 'id' => $openHouse['id']])}}" class="card-link stretched-link"></a>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                </div>

                <div class="row choose mt-2">
                    <div class="col-sm-6 mb-3">
                        <button disabled type="button" class="btn form-control"><img src="{{asset('images/icon-leads-2.png')}}"> Choose Events to View Leads</button>
                    </div>
                    <div class="col-sm-6 mb-3">
                        <button disabled type="button" class="btn form-control"><img src="{{asset('images/icon-report-3.png')}}"> Choose Events to Generate Reports</button>
                    </div>
                </div>

                <hr class="mb-2" style="">

                <center><a id="more" class="view-more">VIEW MORE OPEN HOUSES</a></center>

            </div>
        </div>
        <p class="my-3 footer">&COPY; OTC Open House 2019</p>
    </div>
    <script>
        $(document).ready(function () {
            let moreElements = new showMoreElements('past-openHouse-card', 3);
            $('#more').click(function () {
                moreElements.viewMore(3);
            });
        });
    </script>
</body>
@endsection