<!DOCTYPE html>
<html>
<head>
    <title>Open Houses Add Files</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Custom CSS-->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Bootstrap for responsive layout-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

    <!-- jQuery files for collapsable pannel-->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>


    <!-- Fonts CSS-->
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">


</head>
<body id="open-houses-add-files" class="main-layout">
<div class="container-fluid content p-3 mt-5">
    <div class="row content-body">
        <div class="col-sm-12">
            <p class="text-center mt-0 mb-2">Open Houses For</p>
            <h2 class="text-center pb-4">6437 KESSLER DRIVE</h2>

            <div class="row upcoming-events">
                <div class="col-sm-12 pb-3">
                    <h6>Upcoming Open Houses</h6>
                </div>
                <div class="col-sm-4 mb-3">
                    <div class="card">
                        <div class="card-header text-center">
                            <p class="card-text">
                                Sunday, May 5
                            </p>
                        </div>
                        <div class="card-body py-2 time">
                            <p class="card-text  text-center font-weight-bold">
                                <i class="far fa-clock"></i> 2PM - 4PM
                            </p>
                        </div>
                        <div class="card-body activity text-center">
                            <a href="open house details.html" class="card-link stretched-link">
                                no activity yet
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4  mb-3">
                    <div class="card">
                        <div class="card-header text-center">
                            <p class="card-text">
                                Sunday, May 5
                            </p>
                        </div>
                        <div class="card-body py-2 time">
                            <p class="card-text  text-center font-weight-bold">
                                <i class="far fa-clock"></i> 2PM - 4PM
                            </p>
                        </div>
                        <div class="card-body activity text-center">
                            <a href="open house details.html" class="card-link stretched-link">
                                no activity yet
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4"></div>
            </div>

            <div class="row add mb-3 mt-1">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header text-center">
                            <i class="fas fa-plus" style="border:1px solid #D93149; border-radius: 360px; padding: 6px; margin-right: 6px; font-size: 10px;"></i> Add an Open House
                        </div>
                        <div class="card-body px-4">
                            <form>
                                <div class="form-group row date">

                                    <div class="col-sm-6 px-3 py-1 mb-2">
                                        <label for="date">Open House Date</label>
                                        <input type="date" value="30 March 2019" id="date" required>
                                    </div>

                                    <div class="col-sm-6 py-1 mb-2" style="border-radius: 4px; background-color: #eeeeee;">
                                        <label for="stime" >Start Time</label>
                                        <input type="input" id="stime" value="2 PM" required>
                                        <label for="etime">End Time</label>
                                        <input type="input" id="etime" value="2 PM" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="housetype" id="left" cl>Open House Type</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="housetype">
                                            <option>Consumer Focused</option>
                                            <option>Broker Focused</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="paddress"  id="left">Property Address</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control" id="paddress">
                                            <option>5222 Cangas Drive</option>
                                            <option>6437 Kessler Drive</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-danger"  id="left">Save Changes</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row past-events">
                <div class="col-sm-12 pb-3">
                    <h6>Past Open Houses</h6>
                </div>
                <div class="col-sm-4 mb-3">
                    <div class="card">
                        <div class="card-header text-center">
                            <p class="card-text">
                                Sunday, April 28
                            </p>
                        </div>
                        <div class="card-body py-2 time">
                            <p class="card-text  text-center font-weight-bold">
                                <i class="far fa-clock"></i> 2PM - 4PM
                            </p>
                        </div>
                        <div class="card-body activity text-center px-2 pt-3 pb-0">
                            <p class="card-text">
                                <img src="images/icon-check-in.png"><br>
                                <span class="num">23</span><br>
                                <span class="name">Check-ins</span>
                            </p>
                            <p class="card-text">
                                <img src="images/icon-check-out.png"><br>
                                <span class="num">15</span><br>
                                <span class="name">Check-outs</span>
                            </p>
                            <p class="card-text">
                                <img src="images/icon-text-msg-2.png" style="width: 20px;"><br>
                                <span class="num">6</span><br>
                                <span class="name">Comments</span>
                            </p>
                            <a href="open house details.html" class="card-link stretched-link"></a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4  mb-3">
                    <div class="card">
                        <div class="card-header text-center">
                            <p class="card-text">
                                Sunday, April 28
                            </p>
                        </div>
                        <div class="card-body py-2 time">
                            <p class="card-text  text-center font-weight-bold">
                                <i class="far fa-clock"></i> 2PM - 4PM
                            </p>
                        </div>
                        <div class="card-body activity text-center px-2 pt-3 pb-0">
                            <p class="card-text">
                                <img src="images/icon-check-in.png"><br>
                                <span class="num">23</span><br>
                                <span class="name">Check-ins</span>
                            </p>
                            <p class="card-text">
                                <img src="images/icon-check-out.png"><br>
                                <span class="num">15</span><br>
                                <span class="name">Check-outs</span>
                            </p>
                            <p class="card-text">
                                <img src="images/icon-text-msg-2.png" style="width: 20px;"><br>
                                <span class="num">6</span><br>
                                <span class="name">Comments</span>
                            </p>
                            <a href="open house details.html" class="card-link stretched-link"></a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4  mb-3">
                    <div class="card">
                        <div class="card-header text-center">
                            <p class="card-text">
                                Sunday, April 28
                            </p>
                        </div>
                        <div class="card-body py-2 time">
                            <p class="card-text  text-center font-weight-bold">
                                <i class="far fa-clock"></i> 2PM - 4PM
                            </p>
                        </div>
                        <div class="card-body activity text-center px-2 pt-3 pb-0">
                            <p class="card-text">
                                <img src="images/icon-check-in.png"><br>
                                <span class="num">23</span><br>
                                <span class="name">Check-ins</span>
                            </p>
                            <p class="card-text">
                                <img src="images/icon-check-out.png"><br>
                                <span class="num">15</span><br>
                                <span class="name">Check-outs</span>
                            </p>
                            <p class="card-text">
                                <img src="images/icon-text-msg-2.png" style="width: 20px;"><br>
                                <span class="num">6</span><br>
                                <span class="name">Comments</span>
                            </p>
                            <a href="open house details.html" class="card-link stretched-link"></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row choose mt-2">
                <div class="col-sm-6 mb-3">
                    <button type="button" class="btn form-control"><img src="images/icon-leads-2.png"> Choose Events to View Leads</button>
                </div>
                <div class="col-sm-6 mb-3">
                    <button type="button" class="btn form-control"><img src="images/icon-report-3.png"> Choose Events to Generate Reports</button>
                </div>
            </div>

            <hr class="mb-2" style="">

            <center><a href="#" id="more">VIEW MORE OPEN HOUSES</a></center>

        </div>
    </div>
    <p class="my-3 footer">all rights are reserved to OTC Open House 2019</p>
</div>
</body>
</html>