<html>
<head>
    <title>OTC Open House App Powered By Offer To Close</title>
    <meta name="keywords" content="Open House Service, Open House App, OTC Open House, OTCOpenHouse.com, real estate assistant, offer to close, offertoclose.com" />
    <meta name="description" content="OTC Open House is a mobile app to help make the open house a better tool for agents to track their properties, open houses, and leads created from their open house." />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/prelogin.css')}}">

    <!-- Bootstrap for responsive layout-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap_v3.3.7.min.css')}}">

    <!-- Scripts -->
    <script src="{{asset('js/jquery_v1.11.2.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!-- Fonts CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
          crossorigin="anonymous">

</head>

<body>

<div class="navbar  ">
    <div class="container">
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><img src="{{asset('images/prelogin_logo.png')}}"></a>

            <button class="navbar-toggle collapse_btn" data-toggle="collapse" data-target=".nav1">
                <span class="icon-bar" ></span>
                <span class="icon-bar" ></span>
                <span class="icon-bar" ></span>
            </button>
        </div>

        <div class="collapse navbar-collapse nav1 ">
            <ul class="nav navbar-nav">
                <li><a href="#About_Us">ABOUT US</a></li>
                <li>
                    <a href="#" class="dropdown-toggle" id="dropdownProducts" data-toggle="dropdown">OUR PRODUCTS</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownProducts">
                        <li><a href="https://www.offertoclose.com/">Transaction Management</a></li>
                        <li><a href="https://www.offertoclose.com/offers">Offers App</a></li>
                        <li><a href="https://www.offertoclose.com/transaction-timeline">Transaction Timeline</a></li>
                    </ul>
                </li>
                <li><a href="#Contact_Us">CONTACT US</a></li>
                <li><a href="{{route('register')}}">REGISTER</a></li>
                <li><a href="{{route('login')}}">SIGN IN</a></li>
                <li><a href="tel:8336333782" style="color: #CE343E;">(833) OFFER-TC</a></li>
            </ul>
        </div>
    </div>
</div>

<section class="container-fluid" id="what">
    <div class="container">
        <div class="row" style="max-width: 1000px; margin: 0 auto;">
            <div class="col-sm-4">
                <img src="{{asset('images/prelogin_what-mob.png')}}" class="img-responsive" style="max-width: 400px; max-height: 500px">
            </div>
            <div class="col-sm-8" style="padding: 70px 40px 30px; ">
                <h1>OTC Open House App</h1>
                <p>OTC Open House is a mobile app to help make the open house a better tool for agents to track their properties, open houses and leads created from their open house. Unlike traditional means of gathering information, this platform will allow customers or agents to input information directly into the OTC system for better organization and follow-up.</p>
                <button class="btn" type="button"><img src="{{asset('images/prelogin_app_store.png')}}" class="img-responsive"></button>
                <button class="btn" type="button"><img src="{{asset('images/prelogin_google_play.png')}}" class="img-responsive"></button>
            </div>

        </div>
    </div>
</section>

<section class="container-fluid" id="how">
    <div class="container">
        <h1 class="text-center">How it Works</h1>
        <div class="row" style="max-width: 1100px; margin: 0 auto;">
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-1.png')}}" class="img-responsive">
                    <p>Track open<br>house leads</p>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-2.png')}}" class="img-responsive">
                    <p>Fast and effective reporting for each open house</p>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-3.png')}}" class="img-responsive">
                    <p>Get real-time feedback from open house visitors</p>
                </center>
            </div>
            <div class="col-sm-3">
                <center>
                    <img src="{{asset('images/prelogin_icon-4.png')}}" class="img-responsive">
                    <p>Simple follow-up with email, text, or phone calls</p>
                </center>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-7" style="padding-right: 0;"><a id="About_Us"> </a>
                <h2>About Offer To Close</h2>
                <p>At Offer To Close, our mission is to create tools and services that make the home-buying process simple, transparent, and affordable and that extends to making the entire open house experience simpler and more efficient.  We created OTC Open House to add one more arrow in the agent&#39;s quiver while also increasing the value proposition that we uniquely offer agents with our transaction management platform, experienced transaction coordinators, and tech services like OTC Open House <br><br>OTC Open House apps will be available to download soon for iOS on Apple&#39;s&reg; App Store and for Android&trade; users on the Google Play&trade; Store.</p>
                <button class="btn" type="button"><img src="{{asset('images/prelogin_app_store2.png')}}" class="img-responsive"></button>
                <button class="btn" type="button"><img src="{{asset('images/prelogin_google_play2.png')}}" class="img-responsive"></button>
            </div>
            <div class="col-sm-5" style="padding-top: 150px; padding-left: 0px;">
                <img src="{{asset('images/prelogin_about-mob.png')}}" class="img-responsive">
            </div>
        </div>
    </div>
</section>

<section class="container-fluid" id="contact">
    <div class="container"><a id="Contact_Us"> </a>
        <h1>Contact Us</h1>
        <div class="row" style="max-width: 1100px; margin: 0 auto;">
            <div class="col-sm-6">
                <form>
                    <input type="text" name="uname" placeholder="Name" class="form-control">
                    <input type="email" name="email" placeholder="Email"  class="form-control">
                    <input type="text" name="subject" placeholder="subject"  class="form-control">
                    <textarea name="message" placeholder="Message"  class="form-control"></textarea>
                    <button class="btn" type="button" class="form-control"><img src="{{asset('images/prelogin_send.png')}}" class="img-responsive"></button>
                </form>
            </div>
            <div class="col-sm-6">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3300.9546540313286!2d-118.56103578449382!3d34.17307701879943!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2994078f82e73%3A0x9d7aa561b892e88b!2s19525+Ventura+Blvd%2C+Tarzana%2C+CA+91356!5e0!3m2!1sen!2sus!4v1553638496921" width="500" height="330" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<footer>
    <center><img src="{{asset('images/prelogin_logo2.png')}}" class="img-responsive"></center>
</footer>


</body>
</html>