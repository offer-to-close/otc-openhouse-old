@extends('1a.layouts.prelogin')
@section('content')
<body id="Open_House_Sign_Out" class="open-house">
    <!-- Banner -->
    <div class="container-fluid banner">
        <div class="row">
            <div class="col-sm-12">
                <a href="{{route('logout.user', ['url' => \App\Library\Utilities\_Crypt::base64url_encode(route('get.page', ['name' => 'openHouses']))])}}"><button class="btn" id="close">X</button></a>
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <!-- Body Content -->
                <div class="col-sm-12 content p-5 my-4">
                    <center>
                        <h1>Check-out to {{$data['address']}}</h1>
                        <p class="pb-4 pt-2" id="p1">Agent: {{$data['agent_name']}}</p>

                        <form action="{{route('lead.signOut')}}" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group row">

                                <div class="col-sm-6">
                                    <div class="col-sm-12 mb-3">
                                        <?php
                                            $signedInLeads = $data['leads'];
                                        ?>
                                        <label for="fname" class="float-left mb-1">Name</label>
                                        <select class="form-control" name="id">
                                            @foreach($signedInLeads as $lead)
                                                <option value="{{$lead['id']}}">{{$lead['name']}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('id'))
                                            <span class="help-block">{{$errors->first('id')}}</span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12 mb-3">
                                        <?php
                                            $propertyRatings = [
                                                10 => '10 - This house is amazing',
                                                9 => '9',
                                                8 => '8',
                                                7 => '7',
                                                6 => '6',
                                                5 => '5 - I don\'t hate it, but I don\'t love it',
                                                4 => '4',
                                                3 => '3',
                                                2 => '2',
                                                1 => '1 - I would never live here',
                                            ];
                                        ?>
                                        <label for="lname" class="float-left mb-1">How Would You Rate This Property?</label>
                                        <select class="form-control" name="rating">
                                            @foreach($propertyRatings as $value => $display)
                                                @if($value == old('rating'))
                                                    <option value="{{$value}}" selected>{{$display}}</option>
                                                @else
                                                    <option value="{{$value}}">{{$display}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if($errors->has('rating'))
                                            <span class="help-block">{{$errors->first('rating')}}</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 mb-3">
                                        <?php
                                            $priceOpinions = [
                                                'right' => 'The Price is Right',
                                                'low'   => 'Too Low',
                                                'high'  => 'Too High',
                                            ]
                                        ?>
                                        <label for="lname" class="float-left mb-1">The Price of This Property is...</label>
                                        <select class="form-control" name="price_of_property">
                                            @foreach($priceOpinions as $value => $display)
                                                @if($value == old('price_of_property'))
                                                    <option value="{{$value}}" selected>{{$display}}</option>
                                                @else
                                                    <option value="{{$value}}">{{$display}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if($errors->has('price_of_property'))
                                            <span class="help-block">{{$errors->first('price_of_property')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-12 mb-3">
                                        <?php
                                            $interestLevels = [
                                                10 => '10 - You Should Expect an Offer',
                                                9 => '9',
                                                8 => '8',
                                                7 => '7',
                                                6 => '6',
                                                5 => '5 - I\'m Gonna Keep Looking',
                                                4 => '4',
                                                3 => '3',
                                                2 => '2',
                                                1 => '1 - Not at All',
                                            ];
                                        ?>
                                        <label for="fname" class="float-left mb-1">How Interested Are You In This Property?</label>
                                        <select class="form-control" name="interested_in_property">
                                            @foreach($interestLevels as $value => $display)
                                                @if($value == old('interested_in_property'))
                                                    <option value="{{$value}}" selected>{{$display}}</option>
                                                @else
                                                    <option value="{{$value}}">{{$display}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if($errors->has('interested_in_property'))
                                            <span class="help-block">{{$errors->first('interested_in_property')}}</span>
                                        @endif
                                    </div>

                                    <div class="col-sm-12 mb-3">
                                        <label for="lname" class="float-left mb-1">Any Other Feedback?</label>
                                        <textarea style="height: 130px;" class="py-2 px-2" name="feedback"></textarea>
                                    </div>
                                </div>

                            </div>

                            <a href="{{route('logout.user', ['url' => \App\Library\Utilities\_Crypt::base64url_encode(route('get.page.details', ['name' => 'openHouses', 'id' => $data['id']]))])}}"><button type="button" class="btn btn-outline-danger mr-2 mt-4" id="btn-back">Back</button></a>
                            <button class="btn btn-danger mt-4" id="btn-save">Sign Out</button>

                        </form>
                    </center>
                </div>
            </div>
            <p id="rights">&copy; OTC Open House 2019</p>
        </div>

    </div>
</body>
@endsection