@extends('1a.layouts.master')
@section('content')
<body id="home" class="main-layout">
    <div class="container-fluid content p-3 mt-2">
        <div class="row py-5 content-body">
            <div class="col-sm-12">
                <center>
                    <?php
                        $photo = \App\Http\Controllers\UserController::returnCorrectProfilePhoto();
                    ?>
                    @if($photo)
                        <img src="{{$photo}}" class="img-responsive user_profile_picture">
                    @endif
                    <h2>Welcome, {{auth()->user()->first_name}}</h2>
                    <p class="mt-3 mb-4">How can we help you today?</p>
                    <a href="{{route('get.page', ['name' => 'leads'])}}">
                        <button class="btn">
                            <img src="{{asset('images/icon-leads.png')}}"><br> View Leads
                        </button>
                    </a>
                    <a href="{{route('get.page', ['name' => 'properties'])}}">
                        <button class="btn">
                            <img src="{{asset('images/icon-properties.png')}}"><br> My Properties
                        </button>
                    </a>
                    <a href="{{route('get.page.add',['name' => 'properties'])}}">
                        <button class="btn">
                            <img src="{{asset('images/icon-add-np.png')}}"><br> Add a New Property
                        </button>
                    </a>
                </center>
            </div>
        </div>
        <p class="my-3 footer">all rights are reserved to OTC Open House 2019</p>
    </div>
</body>
@endsection