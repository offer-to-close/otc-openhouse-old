<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('1a.prelogin.welcome'));
});

Auth::routes();
Route::post('/nexmo/verifyNumber', 'NexmoAPI@verifyPhone')->name('nexmo.verifyNumber');
Route::post('/nexmo/verifyCode', 'NexmoAPI@verifyCodeAjax')->name('nexmo.verifyCode');
Route::post('/nexmo/cancel', 'NexmoAPI@cancelVerification')->name('nexmo.cancel');
Route::post('/nexmo/resend', 'NexmoAPI@resendVerification')->name('nexmo.resend');

Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Route::get('/{name}', [
        'as' => 'get.page',
        'uses' => 'HomeController@index'
    ]
)->where('name', 'clients|home|leads|openHouses|properties|account');

Route::get('/{name}/add', [
        'as' => 'get.page.add',
        'uses' => 'HomeController@add'
    ]
)->where('name', 'properties|clients');

Route::get('/{name}/{id}/details', [
    'as' => 'get.page.details',
    'uses' => 'HomeController@details'
])->where('name', 'clients|leads|openHouses|properties');

Route::group(['middleware' => 'auth'], function(){
    Route::post('/user/update', 'UserController@update')->name('user.update');
    Route::get('/user/logout/{url?}', 'UserController@logout')->name('logout.user');
    Route::get('/openHouse/signIn/{id}', 'OpenHouseController@signIn')->name('openHouse.signIn');
    Route::get('/openHouse/signOut/{id}', 'OpenHouseController@signOut')->name('openHouse.signOut');
    Route::get('/filestack/signature', 'FileController@getFilestackSignature')->name('filestack.signature');
    Route::get('/openHouses/property/{id}', 'PropertyController@viewOpenHouses')->name('property.openHouses');
    Route::post('/openHouse/add/new','OpenHouseController@add')->name('openHouse.add');
    Route::post('/property/add/new', 'PropertyController@add')->name('property.add');
    Route::post('/client/add/new', 'ClientController@add')->name('client.add');
    Route::post('/lead/save', 'LeadController@update')->name('lead.update');
    Route::post('/lead/signIn', 'LeadController@signIn')->name('lead.signIn');
    Route::post('/lead/signOut', 'LeadController@signOut')->name('lead.signOut');
});

